<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'auth';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/*
 * Mitra
 */
$route['scheme/(:any)'] = 'admin/mitra/scheme/$1';
$route['product/(:any)'] = 'admin/mitra/product/$1';

/*
 *Admin 
 */
$route['dashboard'] = 'admin/dashboard';
$route['sample'] = 'admin/dashboard/sample';
$route['settings/(:any)'] = 'admin/settings/$1';

$route['sliders/(:any)'] = 'admin/sliders/$1';
$route['site_logo/(:any)'] = 'admin/site_logo/$1';
$route['advertisements/(:any)'] = 'admin/advertisements/$1';
$route['profile/(:any)'] = 'admin/profile/$1';
$route['user_services/(:any)'] = 'admin/user_services/$1';
$route['wallet'] = 'admin/dashboard/wallet';
$route['member_list'] = 'admin/mitra/member_list';
$route['video/(:any)'] = 'admin/video/$1';
$route['scheme_members/(:any)'] = 'admin/mitra/scheme_members/$1';
$route['winners/(:any)'] = 'admin/mitra/winners/$1';
$route['gift/(:any)'] = 'admin/mitra/gift/$1';
$route['paid_unpaid_list/(:any)'] = 'admin/mitra/paid_unpaid_list/$1';
$route['paid_unpaid_member_list/(:any)'] = 'admin/mitra/paid_unpaid_member_list/$1';
$route['remove_member'] = 'admin/mitra/remove_member';
/*Employees*/
$route['employee/(:any)'] = 'admin/employee/$1';
$route['role/(:any)'] = 'admin/role/$1';
$route['emp_list/(:any)'] = 'admin/emp_list/$1';

/*Payment*/
$route['wallet_transactions/(:any)'] = 'payment/wallet_transactions/$1';
