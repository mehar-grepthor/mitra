<?php

class Video_model extends MY_Model
{
    public $rules;
    public $foreign_key;
    public function __construct()
    {
        parent::__construct();
        $this->table = 'videos';
        $this->primary_key = 'id';
      
        
       $this->_config();
       $this->_form();
       $this->_relations();
    }
    
   
    
    public function _config() {
        $this->timestamps = TRUE;
        $this->soft_deletes = TRUE;
        $this->delete_cache_on_save = TRUE;
    }
    
    public function _relations(){
      
    }
    
   
    
    public function _form(){
        $this->rules = array(
            array(
                'field' => 'title',
                'lable' => 'Title',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => 'You must provide a title of video.',
                   
                )
            ),
           
        );
    }
}

