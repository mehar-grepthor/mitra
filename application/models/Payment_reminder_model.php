<?php

class Payment_reminder_model extends MY_Model
{
    public $rules;
    public function __construct()
    {
        parent::__construct();
        $this->table = 'payment_reminders';
        $this->primary_key = 'id';
        
       $this->_config();
       $this->_form();
       $this->_relations();
    }
    public function _config() {
        $this->timestamps = FALSE;
        $this->soft_deletes = FALSE;
        $this->delete_cache_on_save = TRUE;
    }
    
    public function _relations(){
        $this->has_one['user'] = array('User_model', 'id', 'user_id'); 
        $this->has_one['plan'] = array('Plan_model', 'id', 'plan_id'); 
        $this->has_one['plan_details'] = array('Plan_details_model', 'id', 'plan_details_id'); 
    }
    
   public function _form(){
        
    }
    public function all($scheme_id = NULL, $user_id = NULL, $date_from = NULL, $date_to = NULL)
    {
        $this->_query_all($scheme_id, $user_id, $date_from, $date_to);
        $this->db->order_by('id', 'DESC');
        $rs     = $this->db->get();
        $result = $rs->result_array();
        return  $result;
        
    }
    private function _query_all($scheme_id = NULL, $user_id = NULL, $start_date = NULL, $date_to = NULL)
    {
        $this->load->model(array('User_model', 'Scheme_model'));
        $primary_key = '`' . $this->primary_key . '`';
        $table       = '`' . $this->table . '`';
        
        $user_table       = '`' . $this->user_model->table . '`';
        $user_primary_key = '`' . $this->user_model->primary_key . '`';
        $user_foreign_key = '`' . 'user_id' . '`';
        
        $scheme_table       = '`' . $this->scheme_model->table . '`';
        $scheme_primary_key = '`' . $this->scheme_model->primary_key . '`';
        $scheme_foreign_key = '`' . 'scheme_id' . '`';
        
        $str_select_product = '';
       
        foreach (array( 'id', 'user_id',  'scheme_id', 'due_date', 'status') as $v)
        {
            $str_select_product .= "$table.`$v`,";
        }
        $this->db->select($str_select_product."$user_table.`id` as user_id,". "$user_table.`unique_id` as unique_id,"."$user_table.`phone` as mobile,"."$user_table.`email` as email,"."$scheme_table.`name` as scheme_name," );
        $this->db->from($table);
        $this->db->join($user_table, "$user_table.$primary_key=$table.$user_foreign_key", 'left');
        $this->db->join($scheme_table,"$scheme_table.$primary_key=$table.$scheme_foreign_key", 'left');
        if (! empty($date_from) && ! empty($date_to)){
            $this->db->or_where('date(`payment_reminders`.`due_date`) BETWEEN "'. date('Y-m-d', strtotime($date_from)). '" and "'. date('Y-m-d', strtotime($date_to)).'"');
        }elseif (! empty($date_from) &&  empty($date_to)){
            $this->db->or_where("date($table.`due_date`)=",  date('Y-m-d', strtotime($date_from)));
        }
        
        if(! empty($user_id)){
            $this->db->where("$table.`user_id`=", $user_id);
        }
        $this->db->where("$table.`scheme_id`=", $scheme_id);
        
        return $this;
    }
}

