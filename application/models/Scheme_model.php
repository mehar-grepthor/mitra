<?php

class Scheme_model extends MY_Model
{
    public $rules;
    public function __construct()
    {
        parent::__construct();
        $this->table = 'schemes';
        $this->primary_key = 'id';
        $this->before_create[] = '_add_created_by';
        $this->before_update[] = '_add_updated_by';
        
       $this->_config();
       $this->_form();
       $this->_relations();
    }
    
    protected function _add_created_by($data)
    {
        $data['created_user_id'] = $this->ion_auth->get_user_id(); //add user_id
        return $data;
    }
    
    protected function _add_updated_by($data)
    {
        $data['updated_user_id'] = $this->ion_auth->get_user_id(); //add user_id
        return $data;
    } 
    
    public function _config() {
        $this->timestamps = TRUE;
        $this->soft_deletes = TRUE;
        $this->delete_cache_on_save = TRUE;
    }
    
    public function _relations(){
       
       
        $this->has_many['products'] = array(
            'foreign_model' => 'product_model',
            'foreign_table' => 'products',
            'local_key' => 'id',
            'foreign_key' => 'scheme_id',
            'get_relate' => FALSE
        );
        
//         $this->has_many_pivot['scheme_members'] = array(
//             'foreign_model' => 'user_model',
//             'pivot_table' => 'user_schemes',
//             'foreign_key' => 'id',
//             'local_key' => 'id',
//             'pivote_local_key' => 'scheme_id',
//             'pivote_foreign_key' => 'user_id',
//             'get_relate' => FALSE
//         );
       
        $this->has_many_pivot ['scheme_winner'] = array (
            'foreign_model' => 'user_model',
            'pivot_table' => 'user_schemes',
            'local_key' => 'id',
            'pivot_local_key' => 'scheme_id',
            'pivot_foreign_key' => 'user_id',
            'foreign_key' => 'id',
            'get_relate' => FALSE
        );
        $this->has_many['member'] = array(
            'foreign_model' => 'user_model',
            'foreign_table' => 'users',
            'local_key' => 'id',
            'foreign_key' => 'scheme_id',
            'get_relate' => FALSE
        );
    }
    
   
    
    public function _form(){
        $this->rules = array(
            array(
                'field' => 'name',
                'lable' => 'Name',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => 'You must provide a name of scheme.',
                   
                )
            ),
           
        );
    }
}

