<?php

class Member_model extends MY_Model
{
    public $rules;
    public function __construct()
    {
        parent::__construct();
        $this->table = 'members';
        $this->primary_key = 'id';
        
       $this->_config();
       $this->_form();
       $this->_relations();
    }
    public function _config() {
        $this->timestamps = TRUE;
        $this->soft_deletes = TRUE;
        $this->delete_cache_on_save = TRUE;
    }
    public function _relations(){
        $this->has_one['user'] = array('User_model', 'id', 'user_id'); 
        $this->has_one['parent'] = array('User_model', 'id', 'parent_user_id'); 
        $this->has_many_pivot ['groups'] = array (
            'foreign_model' => 'Group_model',
            'pivot_table' => 'users_groups',
            'local_key' => 'id',
            'pivot_local_key' => 'user_id',
            'pivot_foreign_key' => 'group_id',
            'foreign_key' => 'id',
            'get_relate' => FALSE
        );
       
    }
   public function _form(){
        $this->rules = array(
            array(
                'field' => 'ref_id',
                'lable' => 'Referal Id',
                'rules' => 'callback_check_referance',
                'errors' => array(
                    'min_length' => 'you need to give minimum 8 characters',
                    'check_referance' => 'Referal id is not valid'
                ),
                ),
            array(
                'field' => 'email',
                'lable' => 'Email',
                'rules' => 'required|valid_email|callback_check_email',
                'errors' => array(
                  'check_email' => 'Email already exists!!'  
                ),
            ),
            );
        $this->rules['update'] = array(
            array(
                'field' => 'email',
                'lable' => 'Email',
                'rules' => 'trim|required|valid_email|callback_check_email_update'
            ),
        );
    }
}

?>