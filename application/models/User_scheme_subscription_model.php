<?php

class user_scheme_subscription_model extends MY_Model
{
    public $rules;
    public function __construct()
    {
        parent::__construct();
        $this->table = 'scheme_subscription';
        $this->primary_key = 'id';
        
       $this->_config();
       $this->_form();
       $this->_relations();
    }
    
    private function _config() {
        $this->timestamps = TRUE;
        $this->soft_deletes = TRUE;
        $this->delete_cache_on_save = TRUE;
    }
    
    private function _relations(){
        $this->has_one = array('User_model', 'id', 'user_id');
    }
    
    private function _form(){
        $this->rules = array(
            array (
                'lable' => 'plan_details_id',
                'field' => 'plan_details_id',
                'rules' => 'required',
            ),
            array (
                'lable' => 'txn_id',
                'field' => 'txn_id',
                'rules' => 'required',
            ),
            array (
                'lable' => 'amount',
                'field' => 'amount',
                'rules' => 'required',
            ),
        );
    }
}

