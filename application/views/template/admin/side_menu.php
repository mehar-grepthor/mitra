<div class="main-sidebar sidebar-style-2">
				<aside id="sidebar-wrapper">
					<div class="sidebar-brand">
						<a href="<?php echo base_url()?>dashboard"> <img alt="image" src="<?php echo base_url()?>assets/img/mitra_new.png" class="header-logo" /> 
                            <!--<span class="logo-name">Aegis</span>-->
						</a>
					</div>
					<div class="sidebar-user">
						<div class="sidebar-user-picture">
							<img alt="image" src="<?php echo base_url()?>assets/img/userbig.png" >
						</div>
						<div class="sidebar-user-details">
							<div class="user-name"><?php echo 'Mitra';?></div>
							<div class="user-role"><?php echo 'Admin';?></div>
						</div>
					</div>
					<ul class="sidebar-menu">
						<li class="menu-header">Main</li>
						<li class="dropdown active"><a href="<?php echo base_url('dashboard');?>" class="nav-link "><i
									data-feather="monitor"></i><span>Dashboard</span>
							</a>
							
						</li>
						<li class="dropdown "><a href="<?php echo base_url('member_list');?>" class="nav-link "><i data-feather="monitor"></i><span>Members</span></a>
    					</li>
						<!-- Scheme -->
							<li class="dropdown"><a href="#" class="nav-link has-dropdown"><i data-feather="copy"></i><span>Master Data</span></a>
    							<ul class="dropdown-menu">
        								<li><a class="nav-link" href="<?php echo base_url('scheme/r');?>">Schemes</a></li>
        								<li><a class="nav-link" href="<?php echo base_url('product/r');?>">Products</a></li>
    				            </ul>
    						</li>
    						<li class="dropdown "><a href="<?php echo base_url('wallet_transactions/list');?>" class="nav-link "><i data-feather="monitor"></i><span>Transactions</span>
    							</a>
    						</li>
    						<li class="dropdown"><a href="#" class="nav-link has-dropdown"><i data-feather="copy"></i><span>Settings</span></a>
    							<ul class="dropdown-menu">
        								<li><a class="nav-link" href="<?php echo base_url('settings/r');?>">Site Settings</a></li>
    									<li><a class="nav-link" href="<?php echo base_url('sliders/r');?>">Manage Sliders</a></li>
    									<li><a class="nav-link" href="<?php echo base_url('video/r');?>">Manage Videos</a></li>
    				            </ul>
    						</li>
					</ul>
				</aside>
</div>