<?php
(defined('BASEPATH')) or exit('No direct script access allowed');

/**
 *
 * @author Trupti
 *         Admin module
 */
class Mitra extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->template = 'template/admin/main';
        if (! $this->ion_auth->logged_in()) // || ! $this->ion_auth->is_admin()
            redirect('auth/login');
        $this->load->model('scheme_model');
        $this->load->model('product_model');
        $this->load->model('member_model');
        $this->load->model('user_model');
        $this->load->model('gift_model');
        $this->load->model('payment_reminder_model');
        $this->load->model('winners_model');
      
    }
    public function index()
    {
        redirect('admin/dashboard');
    }
    /**
     * schemes crud
     *
     * @author trupti
     * @param string $type
     * @param string $target
     */
    public function scheme($type = 'r')
    {
        /*
         * if (! $this->ion_auth_acl->has_permission('category'))
         * redirect('admin');
         */
        if ($type == 'c') {
            $this->form_validation->set_rules($this->scheme_model->rules);
            if (empty($_FILES['file']['name'])) {
                $this->form_validation->set_rules('file', 'Scheme Image', 'required');
            }
            if ($this->form_validation->run() == FALSE) {
                $this->scheme('r');
            } else {
                $id = $this->scheme_model->insert([
                    'name' => $this->input->post('name'),
                    'desc' => $this->input->post('desc'),
                    'scheme_months' => $this->input->post('scheme_months'),
                    'scheme_members' => $this->input->post('scheme_members'),
                    'price' => $this->input->post('price'),
                    'scheme_sub_charge' => $this->input->post('scheme_sub_charge')
                ]);
                
                $path = $_FILES['file']['name'];
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                $this->file_up("file", "scheme", $id, '', 'no');
                redirect('scheme/r', 'refresh');
            }
        } elseif ($type == 'r') {
            $this->data['title'] = 'Scheme';
            $this->data['content'] = 'admin/mitra/schemes';
            $this->data['categories'] = $this->scheme_model->get_all();

            $this->_render_page($this->template, $this->data);
        } elseif ($type == 'u') {
            $this->form_validation->set_rules($this->scheme_model->rules);
            if ($this->form_validation->run() == FALSE) {
                $this->scheme('r');
            } else {
                $this->scheme_model->update([
                    'id' => $this->input->post('id'),
                    'name' => $this->input->post('name'),
                    'desc' => $this->input->post('desc'),
                    'scheme_months' => $this->input->post('scheme_months'),
                    'scheme_members' => $this->input->post('scheme_members'),
                    'price' => $this->input->post('price'),
                    'scheme_sub_charge' => $this->input->post('scheme_sub_charge')
                ], $this->input->post('id'));

                if ($_FILES['file']['name'] !== '') {
                    $path = $_FILES['file']['name'];
                    $ext = pathinfo($path, PATHINFO_EXTENSION);
                    $this->file_up("file", "scheme", $this->input->post('id'), '', 'no');
                }
                redirect('scheme/r', 'refresh');
            }
        } elseif ($type == 'd') {
            echo $this->scheme_model->delete([
                'id' => $this->input->post('id')
            ]);
        } elseif ($type == 'edit') {
            $this->data['title'] = 'Edit Scheme';
            $this->data['content'] = 'admin/mitra/edit';
            $this->data['type'] = 'scheme';
            $this->data['category'] = $this->scheme_model->where('id', $this->input->get('id'))->get();
            $this->data['i'] = $this->scheme_model->where('file', $this->input->get('file'))->get();
            $this->_render_page($this->template, $this->data);
        }
    }
    /**
     * Product crud
     *
     * @author Trupti
     * @param string $type
     * @param string $target
     */
    public function product($type = 'r')
    {
        /*
         * if (! $this->ion_auth_acl->has_permission('sub_category'))
         * redirect('admin');
         */
        if ($type == 'c') {
            $this->form_validation->set_rules($this->product_model->rules);
            if (empty($_FILES['file']['name'])) {
                $this->form_validation->set_rules('file', 'product Image', 'required');
            }
            if ($this->form_validation->run() == FALSE) {
                $this->product('r');
            } else {
                $id = $this->product_model->insert([
                    'scheme_id' => $this->input->post('scheme_id'),
                    'name' => $this->input->post('name'),
                    'order' => $this->input->post('order'),
                    'desc' => $this->input->post('desc')
                ]);
                $path = $_FILES['file']['name'];
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                $this->file_up("file", "product", $id, '', 'no');
                redirect('product/r', 'refresh');
            }
        } elseif ($type == 'r') {
            $this->data['title'] = 'Product';
            $this->data['content'] = 'admin/mitra/products';
            $this->data['categories'] = $this->scheme_model->get_all();
            $this->data['sub_categories'] = $this->product_model->order_by('id', 'DESC')->get_all();
            $this->_render_page($this->template, $this->data);
            // echo json_encode($this->data);
        } elseif ($type == 'u') {

            $this->form_validation->set_rules($this->product_model->rules);
            if ($this->form_validation->run() == FALSE) {
                echo validation_errors();
            } else {
                $this->product_model->update([
                    'scheme_id' => $this->input->post('scheme_id'),
                    'name' => $this->input->post('name'),
                    'order' => $this->input->post('order'),
                    'desc' => $this->input->post('desc')
                ], $this->input->post('id'));
                if ($_FILES['file']['name'] !== '') {
                    $path = $_FILES['file']['name'];
                    $ext = pathinfo($path, PATHINFO_EXTENSION);
                    $this->file_up("file", "product", $this->input->post('id'), '', 'no');
                }
                redirect('product/r', 'refresh');
            }
        } elseif ($type == 'd') {
            $this->product_model->delete([
                'id' => $this->input->post('id')
            ]);
        } elseif ($type == 'edit') {
            $this->data['title'] = 'Edit Product';
            $this->data['content'] = 'mitra/edit';
            $this->data['type'] = 'product';
            $this->data['sub_categories'] = $this->product_model->order_by('id', 'DESC')
                ->where('id', $this->input->get('id'))
                ->get();
            $this->data['categories'] = $this->scheme_model->order_by('id', 'DESC')->get_all();
            $this->_render_page($this->template, $this->data);
        }
    }
    /**
     * Member List
     *
     * @author Trupti
     * @param string $type
     * @param string $target
     */
    public function member_list($type = 'r')
    {
        $this->data['title'] = 'Member List';
        $this->data['content'] = 'mitra/member_list';
        $this->data['members'] = $this->member_model->order_by('id', 'DESC')->get_all();
        foreach ($this->data['members'] as $key => $val){
            $this->data['members'][$key]['no_of_childs'] = $this->db->query("SELECT COUNT(*) AS count FROM `members` WHERE user_id != parent_user_id AND parent_user_id = ".$val['user_id'])->result_array()[0];
        }
        $this->_render_page($this->template, $this->data);
    }

    /**
     * Gift crud
     *
     * @author Trupti
     * @param string $type
     * @param string $target
     */
    public function gift($type = 'r')
    {
        if ($type == 'c') {
            $this->form_validation->set_rules($this->gift_model->rules);
            if (empty($_FILES['file']['name'])) {
                $this->form_validation->set_rules('file', 'gift Image', 'required');
            }
            if ($this->form_validation->run() == FALSE) {
                $this->gift('r');
            } else {
                $id = $this->gift_model->insert([
                    'user_id' => $this->input->post('id'),
                    'name' => $this->input->post('name'),
                    'desc' => $this->input->post('desc'),
                ]);
                $path = $_FILES['file']['name'];
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                $this->file_up("file", "gift", $id, '', 'no');
                $this->session->set_flashdata('user_det', ['user_id' => $_POST['id']]);
                redirect("gift/r", 'refresh');
            }
        } elseif ($type == 'r') {
            $this->data['title'] = 'Gifts';
            $this->data['content'] = 'admin/mitra/gift';
            $this->data['gifts'] = $this->gift_model->where('user_id', (empty($_GET['id']))?$this->session->flashdata('user_det')['user_id']:$_GET['id'])->get_all();
            $this->_render_page($this->template, $this->data);
        } elseif ($type == 'u') {
            $this->form_validation->set_rules($this->gift_model->rules);
            if ($this->form_validation->run() == FALSE) {
                echo validation_errors();
            } else {
                $this->gift_model->update([
                    'id' => $this->input->post('id'),
                    'name' => $this->input->post('name'),
                    'desc' => $this->input->post('desc')
                ], $this->input->post('id'));
                if ($_FILES['file']['name'] !== '') {
                    $path = $_FILES['file']['name'];
                    $ext = pathinfo($path, PATHINFO_EXTENSION);
                    $this->session->set_flashdata('user_det', ['user_id' => $_POST['id']]);
                    $this->file_up("file", "gift", $this->input->post('id'), '', 'no');
                }
                redirect('gift/r', 'refresh');
            }
        } elseif ($type == 'd') {
            $this->gift_model->delete([
                'id' => $this->input->post('id')
            ]);
        } elseif ($type == 'edit') {
            $this->data['title'] = 'Edit Gift';
            $this->data['content'] = 'mitra/edit';
            $this->data['type'] = 'gift';
            $this->data['gifts'] = $this->gift_model->where('id',  $_GET['id'])->get();
            $this->_render_page($this->template, $this->data);
        }
    }

    /**
     * scheme members crud
     *
     * @author Trupti
     * @param string $type
     */
    public function scheme_members($type = 'r')
    {
        if ($type == 'r') {
            $this->data['title'] = 'Scheme member list';
            $this->data['content'] = 'admin/mitra/scheme_members';
            $this->data['scheme_users'] = $this->user_model->with_scheme_users('fields: id, name', "where: scheme_id=" . $_GET['scheme_id'])->get_all();
            $this->_render_page($this->template, $this->data);
        } elseif($type == 'd') {
            $this->user_model->delete([
                'id' => $this->input->post('id')
            ]);
        }
    }
    /**
     * paid_unpaid_member_list crud
     *
     * @author Trupti
     * @param string $type
     */
    public function paid_unpaid_member_list($type = 'r')
    {
        if ($type == 'r') {
            $this->data['title'] = 'Scheme member list';
            $this->data['content'] = 'admin/mitra/paid_unpaid_list';
            if (isset($_POST['search'])) {
                $user = NULL;
                if(isset($_POST['user_id'])){
                    $user = $this->user_model->where('unique_id', $_POST['user_id'])->as_array()->get()['id'];
                }
                $this->data['users'] = $this->payment_reminder_model->all($_POST['scheme_id'], $user, $_POST['date_from'],(isset($_POST['date_to']))? $_POST['date_to']:NULL);
                $this->session->set_flashdata('member_filters', [
                    'date_from' => $_POST['date_from'],
                    'date_to' => (isset($_POST['date_to'])) ? $_POST['date_to'] : NULL,
                    'scheme_id' => (isset($_POST['scheme_id'])) ? $_POST['scheme_id'] : NULL,
                    'user_id' => (isset($_POST['user_id'])) ? $_POST['user_id'] : NULL,
                ]);
            }else{
                $this->data['users'] = $this->payment_reminder_model->all($_GET['scheme_id']); 
            }
            $this->_render_page($this->template, $this->data);
        }
    }
    /**
     * winners crud
     *
     * @author Trupti
     * @param string $type
     */
    public function winners($type = 'r')
    {
        if ($type == 'r') {
            $this->data['title'] = 'Scheme winners list';
            $this->data['content'] = 'admin/mitra/winners';
            $this->data['scheme_users'] = $this->scheme_model->with_scheme_winner('fields: id,first_name')->with_products('fields:id,name')->where('id', $_GET['scheme_id'])->get();
            $this->data['winner'] = $this->winners_model->get_all();
            $this->data['product'] = $this->product_model->get_all();
            $this->data['member'] = $this->member_model->get_all();
            $this->_render_page($this->template, $this->data);
        }  elseif ($type == 'c') {
            $id = $this->winners_model->insert([
                'scheme_id' => $_POST['scheme_id'],
                'name' => $this->input->post('name'),
                'product_id' => $this->input->post('product_id'),
            ]);
            redirect('winners/r', 'refresh');
            $this->_render_page($this->template, $this->data);
        } 
       
    }
    
}