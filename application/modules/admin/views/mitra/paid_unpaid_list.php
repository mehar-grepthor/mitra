<div class="row">
	<div class="col-12">
		<form action="<?php echo base_url('paid_unpaid_member_list/r');?>"
			method="post" enctype="multipart/form-data">
			<div class="form-row">
				<input type="hidden" name="scheme_id" value="<?php echo empty($_GET['scheme_id'])? $this->session->flashdata('member_filters')['scheme_id']: $_GET['scheme_id'];?>" />
				<div class="form-group col-md-3">
					<label>Id</label> <input type="text" class="form-control"
						name="user_id" placeholder="Id" value="<?php echo empty($this->session->flashdata('member_filters')['user_id'])?'':$this->session->flashdata('member_filters')['user_id'] ?>">
					<div class="invalid-feedback">Id ?</div>
        					<?php echo form_error('name','<div style="color:red">','</div>')?>
        		</div>
				<div class="form-group col-md-3">
					<label>Date From</label> <input type="date" class="form-control"
						name="date_from" required="" value="<?php echo empty($this->session->flashdata('member_filters')['date_from'])?'':$this->session->flashdata('member_filters')['date_from'] ?>">
					<div class="invalid-feedback">New Date From?</div>
        					<?php echo form_error('name','<div style="color:red">','</div>')?>
        		</div>
				<div class="form-group col-md-3">
					<label>Date To</label> <input type="date" class="form-control"
						name="date_to" required="" value="<?php echo empty($this->session->flashdata('member_filters')['date_to'])?'':$this->session->flashdata('member_filters')['date_to'] ?>">
					<div class="invalid-feedback">New Date To?</div>
        				<?php echo form_error('name','<div style="color:red">','</div>')?>
        		</div>
				<div class="form-group col-md-3">
					<button class="btn btn-primary mt-27 " type="submit" name="search">Search</button>
				</div>
		</form>
	</div>
</div>
<div class="card-body">
	<div class="card">
		<div class="card-header">
			<h4>List of Paid members</h4>
		</div>
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-striped table-hover" id="tableExport"
					style="width: 100%;">
					<thead>
						<tr>
							<th>Id</th>
							<th>User Id</th>
							<th>Scheme</th>
							<th>Mobile</th>
							<th>Email</th>
							<th>Due date</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<?php if(!empty($users)):?>
						<?php $sno = 1; foreach ($users as $user):?>
						<?php if($user['status'] == 2):?>
    					<tr>
							<td><?php echo $sno++;?></td>
							<td><?php echo $user['unique_id'];?></td>
							<td><?php echo $user['scheme_name'];?></td>
							<td><?php echo $user['mobile'];?></td>
							<td><?php echo $user['email'];?></td>
							<td><?php echo $user['due_date'];?></td>
							<td><a
								href="<?php echo base_url()?>gift/edit?id=<?php echo $user['id'];?>"
								class=" mr-2  " type="gift"> <i class="fas fa-pencil-alt"></i>
							</a> <a href="#" class="mr-2  text-danger "
								onClick="delete_record(<?php echo $user['id'] ?>, 'gift')"> <i
									class="far fa-trash-alt"></i>
							</a></td>
						</tr>
						<?php endif;?>
    					<?php endforeach;?>
						<?php else :?>
						<tr>
							<th colspan='7'><h3><center>No Paid Members</center></h3></th>
						</tr>
							<?php endif;?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="card-body">
	<div class="card">
		<div class="card-header">
			<h4>List of Unpaid members</h4>
		</div>
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-striped table-hover" id="tableExport1"
					style="width: 100%;">
					<thead>
						<tr>
							<th>Id</th>
							<th>User Id</th>
							<th>Scheme</th>
							<th>Mobile</th>
							<th>Email</th>
							<th>Due date</th>
							<th>Actions</th>

						</tr>
					</thead>
					<tbody>
						<?php if(!empty($users)):?>
						<?php $sno = 1; foreach ($users as $user):?>
						<?php if($user['status'] == 1):?>
    					<tr>
							<td><?php echo $sno++;?></td>
							<td><?php echo $user['unique_id'];?></td>
							<td><?php echo $user['scheme_name'];?></td>
							<td><?php echo $user['mobile'];?></td>
							<td><?php echo $user['email'];?></td>
							<td><?php echo $user['due_date'];?></td>
							<td><a
								href="<?php echo base_url()?>gift/edit?id=<?php echo $user['id'];?>"
								class=" mr-2  " type="gift"> <i class="fas fa-pencil-alt"></i>
							</a> <a href="#" class="mr-2  text-danger "
								onClick="delete_record(<?php echo $user['id'] ?>, 'gift')"> <i
									class="far fa-trash-alt"></i>
							</a></td>
						</tr>
						<?php endif;?>
    					<?php endforeach;?>
						<?php else :?>
						<tr>
							<th colspan='7'><h3><center><i class="fa fa-frown-o"></i>No Unpaid Members</center></i></h3></th>
						</tr>
							<?php endif;?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
