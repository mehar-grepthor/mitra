<!--Add Product And its list-->
<div class="row">
	<div class="col-12">
		<h4>Add Gift</h4>
		<form class="needs-validation" novalidate="" action="<?php echo base_url('gift/c');?>" method="post" enctype="multipart/form-data">
			<div class="card-header">
				<div class="form-row">
					<div class="form-group col-md-4">
						<label>Gift Name</label> <input type="text"
							class="form-control" name="name" placeholder="Gift Name" required="" value="<?php echo set_value('name')?>">
						<div class="invalid-feedback">New Sub_Category Name?</div>
						<?php echo form_error('name','<div style="color:red">','</div>')?>
					</div>
					<input type="hidden" name="id" value="<?php echo (empty($_GET['id']))?$this->session->flashdata('user_det')['user_id']:$_GET['id']?>" />
					<div class="form-group col-md-4">
						<label>Gift Image</label> 
						<input type="file" name="file" required="" value="<?php echo set_value('file')?>"
							class="form-control" onchange="readURL(this);">
<!-- 							<img id="blah" src="#" alt="" > -->
						<div class="invalid-feedback">Upload Image?</div>
						<?php echo form_error('file', '<div style="color:red">', '</div>');?>
					</div>
       				<div class="col col-md-12" >
                    <label for="type" class="col-4 col-form-label">Description</label> 
                      	<textarea id="gift_desc" name="desc" class="ckeditor" rows="15" data-sample-short></textarea>
                      	<?php echo form_error('desc', '<div style="color:red">', '</div>');?>
                    </div>
					<div class="form-group col-md-12">
						<button class="btn btn-primary mt-27 ">Submit</button>
					</div>
				</div>
			</div>
		</form>
		<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>List of Gifts</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Id</th>
									<th>Gift Name</th>
									<th>Description</th>
									<th>Image</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php if(!empty($gifts)):?>
    							<?php $sno = 1; foreach ($gifts as $gift):?>
    								<tr>
    									<td><?php echo $sno++;?></td>
    									<td><?php echo $gift['name'];?></td>
    									<td><?php echo $gift['desc'];?></td>
    									<td width="15%"><img
    										src="<?php echo base_url();?>uploads/gift_image/gift_<?php echo $gift['id'];?>.jpg?<?php echo time();?>"
    										width="50px" style="width: 75px;"></td>
    									<td><a href="<?php echo base_url()?>gift/edit?id=<?php echo $gift['id']; ?>" class=" mr-2  " type="gift" > <i class="fas fa-pencil-alt"></i>
    									</a> <a href="#" class="mr-2  text-danger " onClick="delete_record(<?php echo $gift['id'] ?>, 'gift')"> <i
    											class="far fa-trash-alt"></i>
    									</a></td>
    								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr ><th colspan='6'><h3><center> No Gifts </center></h3></th></tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

