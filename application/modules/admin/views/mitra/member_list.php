	<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>List of Member</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Id</th>
									<th>User Id</th>
									<th>Name</th>
									<th>Number of Childs</th>
									<th>Mobile</th>
									<th>Gifts</th>

								</tr>
							</thead>
							<tbody>
								<?php if(!empty($members)):?>
    							<?php $sno = 1; foreach ($members as $member):?>
    								<tr>
    									<td><?php echo $sno++;?></td>
    									<td><?php echo $member['unique_id'];?></td>
    									<td><?php echo $member['name'];?></td>
    									<td><?php echo $member['no_of_childs']['count'];?></td>
    									<td><?php echo $member['mobile'];?></td>
    									
    									<td><a href="<?php echo base_url()?>gift/c?id=<?php echo $member['user_id'];?>" class="mr-2" type="gift"> <i class="btn btn-primary mt-27 "">Gifts</i>
    									</a></td>
    
    								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr ><th colspan='6'><h3><center>No Members</center></h3></th></tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>