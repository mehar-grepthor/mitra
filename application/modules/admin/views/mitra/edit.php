<?php if($type == 'scheme'){?>
 <!--Edit scheme -->
    <div class="row">
    <div class="col-12">
        <h4>Edit Scheme</h4>
        <form class="needs-validation" novalidate="" action="<?php echo base_url('scheme/u');?>" method="post" enctype="multipart/form-data">
            <div class="card-header">
                <div class="form-group row">
                    <div class="form-group col-md-6">
                        <label>Scheme Name</label>
                        <input type="text" name="name" class="form-control" required="" value="<?php echo $category['name'];?>">
                        <div class="invalid-feedback">Enter Valid Category Name?</div>
                    </div>
                      <div class="form-group col-md-6">
                        <label>Scheme Months</label>
                        <input type="text" name="scheme_months" min="1" class="form-control" required="" value="<?php echo $category['scheme_months'];?>">
                        <div class="invalid-feedback">Enter Valid Scheme Months?</div>
                    </div>
                      <div class="form-group col-md-6">
                        <label>Scheme Members</label>
                        <input type="number" name="scheme_members" min="1" class="form-control" required="" value="<?php echo $category['scheme_members'];?>">
                        <div class="invalid-feedback">Enter Valid Scheme Members?</div>
                    </div>
                      <div class="form-group col-md-6">
                        <label>Scheme Price</label>
                        <input type="number" name="price" min="1" class="form-control" required="" value="<?php echo $category['price'];?>">
                        <div class="invalid-feedback">Enter Scheme Price?</div>
                    </div>
                       <div class="form-group col-md-6">
                        <label>Scheme Charge</label>
                        <input type="number" name="scheme_sub_charge" min="1" class="form-control" required="" value="<?php echo $category['scheme_sub_charge'];?>">
                        <div class="invalid-feedback">Enter Scheme Price?</div>
                    </div>
                    <input type="hidden" name="id" value="<?php echo $category['id'] ; ?>">
					<div class="form-group col-md-6">
                        <label>Upload Image</label>
                        <input type="file" id='input1' name="file" class="form-control" onchange="readURL(this);" value="<?php echo base_url(); ?>uploads/scheme_image/scheme_<?php echo $category['id']; ?>.jpg?<?php echo time();?>" style="width: 200px;">
                        <br>
                        <img id="blah" src="<?php echo base_url(); ?>uploads/scheme_image/scheme_<?php echo $category['id']; ?>.jpg?<?php echo time();?>" style="width: 200px;" />

                        <div class="invalid-feedback">Upload Image?</div>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Description</label>
                         <textarea id="cat_terms"  class="ckeditor" name="desc" rows="10" data-sample-short>
                            <?php echo $category['desc'];?>
                        </textarea>
                          <?php echo form_error('desc', '<div style="color:red">', '</div>');?>
                        <div class="invalid-feedback">Give some Description</div>
                    </div>
                    <div class="form-group col-md-12">
                        <button class="btn btn-primary mt-27 ">Update</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php }elseif($type == 'product'){?>
   <!--Edit product -->
    <div class="row">
    <div class="col-12">
        <h4>Edit Category</h4>
        <form class="needs-validation" novalidate="" action="<?php echo base_url('product/u');?>" method="post" enctype="multipart/form-data">
            <div class="card-header">
                <div class="form-group row">
                    <div class="form-group col-md-4">
                        <label>Category Name</label>
                        <input type="text" name="name" class="form-control" required="" value="<?php echo $sub_categories['name'];?>">
                        <div class="invalid-feedback">Enter Valid Category Name?</div>
                    </div>
                    <input type="hidden" name="id" value="<?php echo $sub_categories['id'] ; ?>">
                    <div class="form-group col-md-4">
                                <label>Category</label>
                                <!-- <input type="file" class="form-control" required="">-->
                                <select class="form-control" name="scheme_id" required="">
                                    <option value="0" selected disabled>select</option>
                                    <?php foreach ($categories as $category):?>
                                      <option value="<?php echo $category['id'];?>" <?php echo ($category['id'] == $sub_categories['scheme_id'])? 'selected': '';?>><?php echo $category['name']?></option>
                                    <?php endforeach;?>
                                </select>
                                <div class="invalid-feedback">Select Category Name?</div>
                      </div>
					<div class="form-group col-md-4">
                        <label>Upload Image</label>
                        <input type="file" id='input1' name="file" class="form-control" onchange="readURL(this);" value="<?php echo base_url(); ?>uploads/product_image/product_<?php echo $sub_categories['id']; ?>.jpg?<?php echo time();?>" style="width: 200px;">
                        <br>
                        <img id="blah" src="<?php echo base_url(); ?>uploads/product_image/product_<?php echo $sub_categories['id']; ?>.jpg?<?php echo time();?>" style="width: 200px;" />
                        <div class="invalid-feedback">Upload Image?</div>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Description</label>
                         <textarea id="cat_terms"  class="ckeditor" name="desc" rows="10" data-sample-short>
                            <?php echo $sub_categories['desc'];?>
                        </textarea>
                          <?php echo form_error('desc', '<div style="color:red">', '</div>');?>
                        <div class="invalid-feedback">Give some Description</div>
                    </div>
                    <div class="form-group col-md-12">
                        <button class="btn btn-primary mt-27 ">Update</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php }elseif($type == 'gift'){?>
<div class="row">
    <div class="col-12">
        <h4>Edit Gift</h4>
        <form class="needs-validation" novalidate="" action="<?php echo base_url('gift/u');?>" method="post" enctype="multipart/form-data">
            <div class="card-header">

                <div class="form-group row">
                    <div class="form-group col-md-4">
                        <label>Gift Name</label>
                        <input type="text" name="name" class="form-control" required="" value="<?php echo $gifts['name'];?>">
                        <div class="invalid-feedback">Enter Valid Gift Name?</div>
                    </div>
                    <input type="hidden" name="id" value="<?php echo (empty($_GET['id']))?$this->session->flashdata('user_det')['user_id']:$_GET['id']?>" />
					
					<div class="form-group col-md-4">
                        <label>Upload Image</label>
                        <input type="file" id='input1' name="file" class="form-control" onchange="readURL(this);" value="<?php echo base_url(); ?>uploads/gift_image/gift_<?php echo $gifts['id']; ?>.jpg?<?php echo time();?>" style="width: 200px;">
                        <br>
                        <img id="blah" src="<?php echo base_url(); ?>uploads/gift_image/gift_<?php echo $gifts['id']; ?>.jpg?<?php echo time();?>" style="width: 200px;" />
                        <div class="invalid-feedback">Upload Image?</div>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Description</label>
                         <textarea id="cat_terms"  class="ckeditor" name="desc" rows="10" data-sample-short>
                            <?php echo $gifts['desc'];?>
                        </textarea>
                          <?php echo form_error('desc', '<div style="color:red">', '</div>');?>
                        <div class="invalid-feedback">Give some Description</div>
                    </div>
                    <div class="form-group col-md-12">
                        <button class="btn btn-primary mt-27 ">Update</button>
                    </div>
                </div>
            </div>

        </form>
    </div>
</div>
<?php }?>