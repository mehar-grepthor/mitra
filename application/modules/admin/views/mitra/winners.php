<!--Add Product And its list-->
<div class="row">
	<div class="col-12">
		<h4>Announce Winner</h4>
		<form class="needs-validation" novalidate="" action="<?php echo base_url('winners/c');?>" method="post" enctype="multipart/form-data">
			<div class="card-header">

				<div class="form-row">
				<input type="hidden" name="scheme_id" value="<?php echo $_GET['scheme_id'];?>" />
						 
					<div class="form-group col-md-4">
						<label>Select Member</label>
						<select class="form-control" name="name" required="" >
								<option value="0" selected disabled>--select--</option>
    							<?php foreach ($scheme_users['scheme_winner'] as $user):?>
    								<option value="<?php echo $user['id'];?>"><?php echo $user['first_name']?></option>
    							<?php endforeach;?>
						</select>
						<div class="invalid-feedback">New Scheme Name?</div>
						<?php echo form_error('cat_id','<div style="color:red>"','</div>');?>
					</div>
				
					<div class="form-group col-md-4">
						<label>Select Product</label>
						<select class="form-control" name="product_id"  >
								<option value="0" selected disabled>--select--</option>
    							<?php foreach ($scheme_users['products'] as $product):?>
    								<option value="<?php echo $product['id'];?>"><?php echo $product['name']?></option>
    							<?php endforeach;?>
						</select>
						<div class="invalid-feedback">New Scheme Name?</div>
						<?php echo form_error('cat_id','<div style="color:red>"','</div>');?>
					</div>
					<div class="form-group col-md-12">
						<button class="btn btn-primary mt-27 ">Submit</button>
					</div>
				</div>


			</div>
		</form>

		<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>List of Winners</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport2"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Id</th>
									<th>Member Name</th>
									<th>Product</th>
								</tr>
							</thead>
							<tbody>
								<?php if(!empty($winner)):?>
    							<?php $sno = 1; foreach ($winner as $user):?>
    								<tr>
    									<td><?php echo $sno++;?></td>
    									<td><?php echo $user['name']?></td>
    									<td><?php echo $user['product_id']?></td>
    									<!--<td><a href="<?php echo base_url()?>product/edit?id=<?php //echo $sub_cat['id'];?>" class=" mr-2  " type="product" > <i class="fas fa-pencil-alt"></i>
    									</a> <a href="#" class="mr-2  text-danger " onClick="delete_record(<?php //echo $sub_cat['id'] ?>, 'product')"> <i
     											class="far fa-trash-alt"></i> -->
<!--     									</a></td> -->
    
    								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr ><th colspan='6'><h3><center>No Winners</center></h3></th></tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>

	</div>
</div>

