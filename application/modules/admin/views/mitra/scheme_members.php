<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>List For Scheme Members</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Id</th>
									<th> Name</th>
									<th>Unique Id</th>
									<th>Mobile</th>
									<th>Actions</th>

								</tr>
							</thead>
							<tbody>
								<?php if(!empty($scheme_users)):?>
    							<?php $sno = 1; foreach ($scheme_users as $scheme_user):?>
    								<tr>
    									<td><?php echo $sno++;?></td>
    									<td><?php echo $scheme_user['first_name'];?></td>
    									<td><?php echo $scheme_user['unique_id'];?></td>
    									<td><?php echo $scheme_user['phone'];?></td>
    									<td><a class=" mr-2" type="scheme_members" > <i class="btn btn-primary mt-27 " onClick="remove_record(<?php echo $scheme_user['id'] ?>, 'scheme_members')">Remove</i>
    									</a></td>
   
    								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr><th colspan='6'><h3><center>No Scheme Members</center></h3></th></tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		