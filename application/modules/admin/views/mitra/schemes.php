<!--Add Category And its list-->
<div class="row">
	<div class="col-12">
		<h4>Add Scheme</h4>
		  
		<form class="needs-validation" novalidate="" action="<?php echo base_url('scheme/c');?>" method="post" enctype="multipart/form-data">
			<div class="card-header">
				<div class="form-group row">
					<div class="form-group col-md-4">
						<label>Scheme Name</label> <input type="text" name="name"
							required="" placeholder="Scheme Name" value="<?php echo set_value('name')?>"
							class="form-control" >
						<div class="invalid-feedback">New Scheme?</div>
						<?php echo form_error('name', '<div style="color:red">', '</div>');?>
					</div>
					<div class="form-group col-md-4">
						<label>Scheme Months</label> <input type="number" name="scheme_months" min="1"
							required="" placeholder="Scheme Months" value="<?php echo set_value('scheme_months')?>"
							class="form-control">
						<div class="invalid-feedback">Please Give Schemes months?</div>
						<?php echo form_error('scheme_months', '<div style="color:red">', '</div>');?>
					</div>
					<div class="form-group col-md-4">
						<label>Scheme Members</label> <input type="number" name="scheme_members" min="1"
							required="" placeholder="Scheme Members" value="<?php echo set_value('scheme_members')?>"
							class="form-control">
						<div class="invalid-feedback">Please Give Scheme Members</div>
						<?php echo form_error('scheme_members', '<div style="color:red">', '</div>');?>
					</div>
					<div class="form-group col-md-4">
						<label>Scheme Price</label> <input type="number" name="price" min="1"
							required="" placeholder="Scheme Price" value="<?php echo set_value('price')?>"
							class="form-control">
						<div class="invalid-feedback">Please give Scheme Price?</div>
						<?php echo form_error('price', '<div style="color:red">', '</div>');?>
					</div>
					<div class="form-group col-md-4">
						<label>Scheme Subscription Charge</label> <input type="number" name="scheme_sub_charge" min="1" 
							required="" placeholder="Scheme Subscription Charge" value="<?php echo set_value('scheme_sub_charge')?>"
							class="form-control">
						<div class="invalid-feedback">Please give Scheme Subscription Charge?</div>
						<?php echo form_error('scheme_sub_charge', '<div style="color:red">', '</div>');?>
					</div>
					
					<div class="form-group col-md-4">
						<label>Upload Image</label> <input type="file" name="file"
							required="" value="<?php echo set_value('file')?>"
							class="form-control" onchange="readURL(this);"> <br> <img id="blah"
							src="#" alt="">
						<div class="invalid-feedback">Please Upload Image?</div>
						<?php echo form_error('file', '<div style="color:red">', '</div>');?>
					</div>
					<div class="col col-md-12" >
                    <label for="type" class="col-4 col-form-label">Description</label> 
                      	<textarea id="beauty_desc" name="desc" class="ckeditor" rows="15" data-sample-short></textarea>
                      	<div class="invalid-feedback">Please Give Description?</div>
                      	<?php echo form_error('desc', '<div style="color:red">', '</div>');?>
                    </div>
					<div class="form-group col-md-2">
						<button type="submit" name="upload" id="upload" value="Apply"
							class="btn btn-primary mt-27 ">Submit</button>
					</div>
				</div>
</div>
			
		</form>

		<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>List of Schemes</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Sno</th>
									<th>Scheme Name</th>
									<th>Scheme Months</th>
									<th>Scheme Members</th>
									<th>Price</th>
									<th>Scheme Subscription Charge</th>
									<th>Description</th>
									<th>Image</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
							<?php if(!empty($categories)):?>
    							<?php  $sno = 1; foreach ($categories as $category): ?>
    								<tr>
									<td><?php echo $sno++;?></td>
									<td><?php echo $category['name'];?></td>
									<td><?php echo $category['scheme_months'];?></td>
									<td><?php echo $category['scheme_members'];?></td>
									<td><?php echo $category['price'];?></td>
									<td><?php echo $category['scheme_sub_charge'];?></td>
									<td><?php echo $category['desc'];?></td>
								
									<td><img
										src="<?php echo base_url();?>uploads/scheme_image/scheme_<?php echo $category['id'];?>.jpg?<?php echo time();?>"
										class="img-thumb"></td>
									<td><a
										href="<?php echo base_url()?>scheme/edit?id=<?php echo $category['id']; ?>"
										class=" mr-2  " type="scheme"> <i class="fas fa-pencil-alt"></i>
									</a> <a href="#" class="mr-2  text-danger "
										onClick="delete_record(<?php echo $category['id'] ?>, 'scheme')">
											<i class="far fa-trash-alt"></i>
									</a></td>

								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr>
									<th colspan='7'><h3>
											<center>No Scheme</center>
										</h3></th>
								</tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>

	</div>
</div>
<script>
    // WRITE THE VALIDATION SCRIPT.
    function isNumber(evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
            return false;

        return true;
    }    
</script>
