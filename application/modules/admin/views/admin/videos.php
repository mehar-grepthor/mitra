<!--Add Product And its list-->
<div class="row">
	<div class="col-12">
		<h4>Add Videos</h4>
		<form class="needs-validation" novalidate="" action="<?php echo base_url('video/c');?>" method="post" enctype="multipart/form-data">
			<div class="card-header">

				<div class="form-row">
					<div class="form-group col-md-4">
						<label>Title</label> <input type="text"
							class="form-control" name="title" placeholder="Title" required="" value="<?php echo set_value('title')?>">
						<div class="invalid-feedback">New Title?</div>
						<?php echo form_error('name','<div style="color:red">','</div>')?>
					</div>
					<div class="form-group col-md-4">
						<label>Video Link</label> <input type="text"
							class="form-control" name="video_link" placeholder="Video Link" required="" value="<?php echo set_value('video_link')?>">
						<div class="invalid-feedback">New Video Link?</div>
						<?php echo form_error('name','<div style="color:red">','</div>')?>
					</div>
						<div class="form-group col-md-4">
						<label>Thumbnail Image</label> 
						<input type="file" name="file" required="" value="<?php echo set_value('file')?>"
							class="form-control" onchange="readURL(this);">
<!-- 							<img id="blah" src="#" alt="" > -->
						<div class="invalid-feedback">Upload Image?</div>
						<?php echo form_error('file', '<div style="color:red">', '</div>');?>
					</div>
					<div class="form-group col-md-4">
						<button class="btn btn-primary mt-27 ">Submit</button>
					</div>
				</div>
			</div>
		</form>

		<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>List of video</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Id</th>
									<th>Title</th>
									<th>Video Link</th>
									<th>Image</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php if(!empty($videos)):?>
    							<?php $sno = 1; foreach ($videos as $video):?>
    								<tr>
    									<td><?php echo $sno++;?></td>
    									<td><?php echo $video['title'];?></td>
    									<td><?php echo $video['video_link'];?></td>
    									<td width="15%"><img
    										src="<?php echo base_url();?>uploads/video_thumbnail_image/video_thumbnail_<?php echo $video['id'];?>.jpg?<?php echo time();?>"
    										width="50px" style="width: 75px;"></td>
    									<td><a href="<?php echo base_url()?>video/edit?id=<?php echo $video['id'];?>" class=" mr-2  " type="video" > <i class="fas fa-pencil-alt"></i>
    									</a> <a href="#" class="mr-2  text-danger " onClick="delete_record(<?php echo $video['id'] ?>, 'video')"> <i
    											class="far fa-trash-alt"></i>
    									</a></td>
    								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr ><th colspan='6'><h3><center>No Videos</center></h3></th></tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>

	</div>
</div>

