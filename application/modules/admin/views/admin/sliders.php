<div class="container">
    <div class="row">
        <div class="col-md-12">
                <section class="card">
                    <header class="card-header">
                        <div class="card-actions">
                            <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                            <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                        </div>
                        <h2 class="card-title">Slides</h2>
                    </header>
                    <div class="card-body">
                         <form id="form_cover" action="<?php echo base_url('sliders/slide');?>" class="needs-validation" novalidate="" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-6">
                            <label>Upload Image</label> 
                            <input type="file" name="slide" required="" value="<?php echo set_value('slide')?>"
                            class="form-control" onchange="readURL(this);">
                            <img id="blah" src="#" alt=""> 
                            <div class="col-sm-9">
                                <button class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                    <hr/>
                        <div class="row">
                            <?php
                            foreach ($sliders as $slide) {
                            ?>
                            <div class="col-md-4" style="margin-top: 20px;">
                                <a href="#" class="mr-2  text-danger " onClick="delete_record(<?php echo $slide['id'] ?>, 'sliders')"> <div class="deleteIcon"><i
                                                class="fa fa-trash"></i></div>
                                        </a>
                                <img src="<?php echo base_url(); ?>uploads/sliders_image/sliders_<?php echo $slide['id']; ?>.<?=$slide['ext'];?>?<?php echo time();?>" alt="slider image" class="img-thumbnail">
                            </div>
                        <?php }?>
                        </div>
                    
            
                </section>
        </div>

    </div>
</div>























