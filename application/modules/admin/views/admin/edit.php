<?php if($type == 'user_services'){?>

                <!--Edit State -->
                <div class="row">
                    <div class="col-12">
                        <h4>Edit Service</h4>
                        <form class="needs-validation" novalidate="" action="<?php echo base_url('user_services/u');?> " method="post" enctype="multipart/form-data">
                            <div class="card-header">
                                <div class="form-row">
                                    <div class="form-group col-md-6">

                                        <label>Service Name</label>
                                        <input type="text" name="name" class="form-control" required="" value="<?php echo $services['name']; ?>">

                                        <div class="invalid-feedback">Enter Valid Service Name?</div>
                                    </div>
                                    <input type="hidden" name="id" value="<?php echo $services['id'] ; ?>">
                                    </br>
                                    <div class="form-group col-md-6">
                                        <button class="btn btn-primary mt-27 ">Update</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <?php }elseif($type == 'video'){?>
                 <div class="row">
                    <div class="col-12">
                        <h4>Edit Video</h4>
                        <form class="needs-validation" novalidate="" action="<?php echo base_url('video/u');?> " method="post" enctype="multipart/form-data">
                            <div class="card-header">
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label>Title</label>
                                        <input type="text" name="title" class="form-control" required="" value="<?php echo $videos['title']; ?>">
                                        <div class="invalid-feedback">Enter Valid Title?</div>
                                    </div>
                                    <input type="hidden" name="id" value="<?php echo $videos['id'] ; ?>">
                                    </br>
                                    <div class="form-group col-md-4">
                                        <label>Video Link</label>
                                        <input type="text" name="video_link" class="form-control" required="" value="<?php echo $videos['video_link']; ?>">
                                        <div class="invalid-feedback">Enter Valid Video Link?</div>
                                    </div>
                                    <div class="form-group col-md-4">
                        <label>Upload Image</label>
                        <input type="file" id='input1' name="file" class="form-control" onchange="readURL(this);" value="<?php echo base_url(); ?>uploads/video_thumbnail_image/video_thumbnail_<?php echo $videos['id']; ?>.jpg?<?php echo time();?>" style="width: 200px;">
                        <br>
                        <img id="blah" src="<?php echo base_url(); ?>uploads/video_thumbnail_image/video_thumbnail_<?php echo $videos['id']; ?>.jpg?<?php echo time();?>" style="width: 200px;" />

                        <div class="invalid-feedback">Upload Image?</div>
                    </div>
                                    <div class="form-group col-md-4">
                                        <button class="btn btn-primary mt-27 ">Update</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                  <?php }?>