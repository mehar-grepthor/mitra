<div class="row">
	<?php foreach ($schemes as $scheme):?>
	<div class="col-xl-3  col-md-4 col-sm-4">
			<div class="card">
				<div class="card-bg">
					<div class="p-t-20 d-flex justify-content-between">
						<div class="col">
							<h6 class="mb-0"><?php echo $scheme['name']?></h6>
							<span class="font-weight-bold mb-0 font-20"></span>
						</div>
					</div>
					<!-- <canvas id="cardChart4" height="80"></canvas> -->
					<br />
					<br />
					<!-- <div class="alert alert-sm alert-primary "><center><b><i class="fas fa-check-circle card-icon font-20 p-r-30 "  title="Paid/Unpaid List" > <?php //echo "Paid/Unpaid List";?></i></b></<br><b><i class="fas fa-times-circle card-icon font-20 p-r-30" title="Members"> <?php //echo "Member's ".$this->db->query('SELECT COUNT(*) AS members FROM user_schemes WHERE scheme_id = 1')->row()->members;?></i></b></center></div> -->
					<div class="table-responsive">
						<table id="mainTable" class="table table-striped">
							<thead>
								<tr>
									<th class="alert btn-sm btn-primary"><a title="Paid/Unpaid List" href="<?php echo base_url('paid_unpaid_member_list/r?scheme_id='.$scheme['id']);?>" style="color:black;font-weight: 700;"><?php echo "Paid / Unpaid List";?></a></th>
									<th class="alert btn-sm btn-warning"><a title="Member List" href="<?php echo base_url('scheme_members/r?scheme_id='.$scheme['id']);?>" style="color:black;font-weight: 700;"><?php echo "Member's";?></a></th>
								</tr>
							</thead>
						</table>
					</div>
					<div class="alert btn-sm btn-warning"><center><b><a title="Announce Winner" href="<?php echo base_url('winners/r?scheme_id='.$scheme['id']);?>"><?php echo "Announce Winner's";?></a></b></center></div>
				</div>
			</div>
	</div>

	<?php endforeach;?>
	</div>
