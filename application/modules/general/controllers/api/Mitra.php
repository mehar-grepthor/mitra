<?php 

require APPPATH . '/libraries/MY_REST_Controller.php';
require APPPATH . '/vendor/autoload.php';

use Firebase\JWT\JWT;

class Mitra extends MY_REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('scheme_model');
        $this->load->model('product_model');
        $this->load->model('sliders_model');
        $this->load->model('member_model');
        $this->load->model('user_model');
        $this->load->model('state_model');
        $this->load->model('district_model');
        $this->load->model('video_model');
        $this->load->model('gift_model');
        $this->load->model('winners_model');
    }
    /**
     * @author Trupti
     * @desc To get list of categories and targeted category as well
     * @param string $target
     */
    public function schemes_get($target = '') {
        //$this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        if(empty($target)){
            $data = $this->scheme_model->order_by('name', 'ASC')->get_all();
            if(! empty($data)){
                for ($i = 0; $i < count($data) ; $i++){
                    $data[$i]['image'] = base_url().'uploads/scheme_image/scheme_'.$data[$i]['id'].'.jpg';
                }
            }
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }else{
            $data = $this->scheme_model->with_products('fields: name, id,desc')->where('id', $target)->get();
            $data['image'] = base_url().'uploads/scheme_image/scheme_'.$data['id'].'.jpg';
            if(! empty($data['products'])){
                for ($i = 0; $i < count($data['products']) ; $i++){
                    $data['products'][$i]['image'] = base_url().'uploads/product_image/product_'.$data['products'][$i]['id'].'.jpg';
                }
            }
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }
    }
    
    /**
     * @author Trupti
     * @desc To get list of sub categories
     * @param string $target
     */
    public function products_get($target = '') {
        if(empty($target)){
            $data = $this->product_model->order_by('name', 'ASC')->get_all();
            if(! empty($data)){
                for ($i = 0; $i < count($data) ; $i++){
                    $data[$i]['image'] = base_url().'uploads/product_image/product_'.$data[$i]['id'].'.jpg';
                }
            }
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }else{
            $data = $this->product_model->where('id', $target)->where('fields:name,desc')->get();
            $data['image'] = base_url().'uploads/product_image/product_'.$data['id'].'.jpg';
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }
    }
    /**
     * @author Trupti
     * @desc To get list of sliders
     * @param string $target
     */
    public function sliders_get()
    {
       
        $sliders = $this->sliders_model->get_all();
        if(! empty($sliders)){
            for ($i = 0; $i < count($sliders) ; $i++){
                $data1[$i]['image'] = base_url().'uploads/sliders_image/sliders_'.$sliders[$i]['id'].'.'.$sliders[$i]['ext'];
            }
            $res['sliders']=$data1;
        }
        $this->set_response_simple(($res == FALSE)? FALSE : $res, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    }
    /**
     * @author Trupti
     * @desc To update profile
     * @param string $target
     */
    public function profile_post($type = 'r'){
        $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        $this->token = $token_data;
        if($type == 'r'){
            $data = $this->member_model->where('user_id', $token_data->id)->with_user('fields: wallet')->get();
            $data['image'] = base_url().'uploads/profile_image/profile_'.$data['id'].'.jpg';
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }elseif($type == 'u'){
            $_POST = json_decode(file_get_contents('php://input'), TRUE);
            $this->form_validation->set_rules($this->member_model->rules['update']);
            if ($this->form_validation->run() == FALSE) {
                $this->set_response_simple(validation_errors(), 'Internal Error Occured..!', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
            } else {
                $this->member_model->update([
                    'user_id' => $token_data->id,
                    'name' => $this->input->post('name'),
                    'mobile' => $this->input->post('mobile'),
                    'email' => $this->input->post('email'),
                    'state_id' => $this->input->post('state_id'),
                    'district_id' => $this->input->post('district_id'),
                    'scheme_id' => $this->input->post('scheme_id'),
                    'aadhar_no' => $this->input->post('aadhar_no'),
                    'pan' => $this->input->post('pan'),
                    'gender' => $this->input->post('gender'),
                    'member_dob' => $this->input->post('member_dob'),
                    'nominee_name' => $this->input->post('nominee_name'),
                    'nominee_aadhar' => $this->input->post('nominee_aadhar'),
                    'nominee_dob' => $this->input->post('nominee_dob'),
                    'relation' => count([$this->input->post('relation')]),
                    'marital_status' => $this->input->post('marital_status'),
                ], 'user_id');
                
                $id = $this->user_model->update([
                    'id' => $token_data->id,
                    'phone' => $this->input->post('mobile'),
                    'email' => $this->input->post('email'),
                ], 'id');
                $this->set_response_simple($id, 'Profile Updated..!', REST_Controller::HTTP_OK, TRUE);
            }
        }elseif ($type == 'reset'){
            $this->form_validation->set_rules($this->user_model->rules['reset']);
            $_POST = json_decode(file_get_contents('php://input'), TRUE);
            if ($this->form_validation->run() == false) {
                $this->set_response_simple(validation_errors(), 'Internal Error Occured..!', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
            } else {
                $identity = $token_data->userdetail->email;
                $change = $this->ion_auth->change_password($identity, $this->input->post('opass'), $this->input->post('npass'));
                if ($change) {
                    $this->set_response_simple($change, 'Password Changed..!', REST_Controller::HTTP_OK, TRUE);
                } else {
                    $this->set_response_simple($change, 'Internal Error Occured..!', REST_Controller::HTTP_CONFLICT, FALSE);
                }
            }
        }
    }
    public function check_email_update($email){
        $parent =  $this->user_model->fields('email')->where(['id' => $this->token->id])->as_object()->get();
        if(! empty($parent->email)){
            if($parent->email === $email){
                return TRUE;
            }else{
                $user = $this->user_model->fields('email')->where(['email' => $email])->as_object()->get();
                if(empty($user)){
                    return TRUE;
                }else{
                    $this->form_validation->set_message('check_email_update', 'Sorry, This email is already used by another user please select another one');
                    return FALSE;
                }
            }
        }else{
            $this->form_validation->set_message('check_email_update', 'Sorry, Please check mail');
            return FALSE;
        }
    }
    /**
     * @author Trupti
     * @desc To states get
     * @param string $target
     * @param string $district_id
     */
    public function states_get($target = '', $district_id = '') {
        //$this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        if(empty($target)){
            $data = $this->state_model->get_all();
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }elseif (! empty($target) && empty($district_id)){
            $data = $this->state_model->with_districts('fields:name,id')->where('id', $target)->get();
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }else{
            $data = $this->district_model->where('id', $district_id)->get();
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }
    }
    /**
     * @author Trupti
     * @desc To videos get
     */
    public function videos_get() {
        $data = $this->video_model->get_all();
        if(! empty($data)){
            for ($i = 0; $i < count($data) ; $i++){
                $data[$i]['image'] = base_url().'uploads/video_thumbnail_image/video_thumbnail_'.$data[$i]['id'].'.jpg';
            }
        }
        $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    }
    
    /**
     * @author Trupti
     * @desc To change_the_status get
     */
    public function change_the_status_get($target){
        $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        $data = $this->gift_model->update([
            'id'=>$target,
            'user_id' => $token_data->id,
            'status' =>2
        ], 'id');
        $this->set_response_simple(($token_data == FALSE) ? FALSE : $data, 'Updated..!', REST_Controller::HTTP_ACCEPTED, TRUE);
    }
    
    /**
     * @author Trupti
     * @desc To winner get
     */
    public function winner_get(){
        $data = $this->winners_model->fields('id,scheme_id')->with_members('fields:first_name')->with_products('fields:name')->get_all();
        $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    }
}
?>