<!DOCTYPE html>
<html lang="en">
<head>
<title>Bootstrap Example</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>


	<!-- <div class="jumbotron text-center">
  <h1>My First Bootstrap Page</h1>
  <p>Resize this responsive page to see the effect!</p> 
</div> -->
	<div class="container">
		<div class="jumbotron text-center">
		<form method="post" action="<?php echo base_url("general/common/member_creation/c");?>" class="needs-validation" enctype="multipart/form-data">
			<div class="row">
				<div class="col-sm-6">
					<label for="person">Upload Person Photo</label><br><br>
						<input type="file" id="person" name="person">
				</div>
				<div class="col-sm-6">
				<label>Upload Shop Photo</label><br><br>
						<input type="file" name="shop">
				</div>
			</div>
		</div>


		<!-- row1-->
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group ">
					<label class="control-label " for="name"> Full Name </label> <input
						class="form-control" id="name" name="name" type="text"
						placeholder="Enter Full Name of Candidate" />
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group ">
					<label class="control-label " for="tel"> Mobile Number </label> <input
						class="form-control" id="name" name="mobile" type="text"
						placeholder="Mobile" />
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group ">
					<label class="control-label " for="email1"> Email id </label> <input
						class="form-control" id="name" name="email" type="text"
						placeholder="Enter your valid mail Id(optional)" />
				</div>
			</div>
		</div>





		<!-- row2 -->

		<div class="row">
			<div class="col-sm-4">
				<div class="form-group ">
					<label class="control-label " for="name"> State </label> <select
						class="select form-control" id="select" name="state_id">
						<option value="0" selected disabled>--select--</option>
						<?php foreach ($states as $state){?>
							<option value="<?php echo $state['id']?>"><?php echo $state['name']?></option>
						<?php }?>
					</select>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group ">
					<label class="control-label " for="name"> District </label> <select
						class="select form-control" id="select" name="district_id">
						<option value="0" selected disabled>--select--</option>
						<?php foreach ($districts as $district){?>
							<option value="<?php echo $district['id']?>"><?php echo $district['name']?></option>
						<?php }?>
					</select>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group ">
					<label class="control-label " for="name"> Plans Select </label> <select
						class="select form-control" id="select" name="plan_id">
						<option value="0" selected disabled>--select--</option>
						<option value="1">Daily</option>
						<option value="2">Weekly</option>
						<option value="3">Mothly</option>
						<option value="4">Mahhila</option>
					</select>
				</div>
			</div>
		</div>



		<!-- row3 -->
		<div class="row">
			<div class="col-sm-6">
				<div class="form-group ">
					<label class="control-label " for="name"> Aadhar Card </label> <input
						class="form-control" id="name" name="aadhar_no" type="text"
						placeholder="" />
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group ">
					<label class="control-label " for="name"> Pan Card </label> <input
						class="form-control" id="name" name="pan" type="text"
						placeholder="" />
				</div>
			</div>
		</div>

		<!-- row 4 --->
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group ">
					<label class="control-label ">Gender</label>
					<div class="row">
						<div class="col-sm-2">
							<div class="radio">
								<label class="radio"> <input name="gender" type="radio"
									value="1" /> Male
								</label>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="radio">
								<label class="radio"> <input name="gender" type="radio"
									value="2" /> Female
								</label>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="radio">
								<label class="radio"> <input name="gender" type="radio"
									value="3" /> Others
								</label>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group ">
					<label class="control-label " for="name"> Date of Brith </label> <input
						class="form-control" id="date" name="member_dob" type="text"
						placeholder="yy/mm/dd" />
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group ">
					<label class="control-label " for="name"> Nominee Name </label> <input
						class="form-control" id="name" name="nominee_name" type="text"
						placeholder="Enter full Name Of Nominee" />
				</div>
			</div>
		</div>

		<!-- row 5 -->

		<div class="row">
			<div class="col-sm-4">
				<div class="form-group ">
					<label class="control-label " for="name"> Relation </label> <select
						class="select form-control" id="select" name="relation">
						<option value="0" selected disabled>--select--</option>
						<option value="1"> father</option>
						<option value="2">husband</option>
						<option value="3">son</option>
						<option value="4">mother</option>
						<option value="5">wife</option>
						<option value="6">daughter</option>
						<option value="7">others</option>
					</select>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group ">
					<label class="control-label " for="name"> Nominee Date Of Birth </label>
					<input class="form-control" id="date" name="nominee_dob"
						type="text" placeholder="dd/mm/yy" />
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group ">
					<label class="control-label " for="name"> Nominee Aadhar </label> <input
						class="form-control" id="name" name="nominee_aadhar" type="text"
						placeholder="" />
				</div>
			</div>
		</div>
		<!-- row 6-->
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group ">
					<label class="control-label " for="name"> Current Address </label>
					<input class="form-control" id="name" name="current_address" type="text"
						placeholder="Enter Full Name of Candidate" />
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group ">
					<label class="control-label " for="name"> Permanent Address </label>
					<input class="form-control" id="name" name="permanent_address" type="text"
						placeholder="Mobile" />
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group ">
					<label class="control-label " for="name"> Marital Status </label>
					<div class="row">
						<div class="col-sm-2">
							<div class="radio">
								<label class="radio"> <input name="marital_status" type="radio"
									value="1" /> single
								</label>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="radio">
								<label class="radio"> <input name="marital_status" type="radio"
									value="2" /> married
								</label>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- row 7-->
			<div class="row">
				<div class="col-sm-4">
					<div class="form-group ">
						<label class="control-label " for="name"> Payment </label> <input
							class="form-control" id="name" name="payment" type="text"
							placeholder="600" />
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group ">
						<label class="control-label " for="name"> Payment Method </label>
						<select class="select form-control" id="select" name="payment_method">
    						<option value="0" selected disabled>--select--</option>
    						<option value="1">cash</option>
    						<option value="2">online</option>
						</select>
					</div>
				</div>

			</div>
			<!-- row 8 upload -->
			<div class="jumbotron text-center">
				<div class="row">
					<div class="col-sm-6">
					<label>Kindly Upload Payment Receipt</label><br><br>
						<input type="file" name="payment">
					</div>
					<div class="col-sm-6">
						<div class="row">

							<div class="col-sm-12">
								<input class="form-control" id="name" name="name" type="text"
									placeholder="Enter your valid refer Id" />
							</div>
							<div class="col-sm-12">
								<label class="mt-5"><input type="checkbox">I Accept TERMS &
									CONDITIONS</label>
							</div>
							<div class="col-sm-12">
								<button type="submit" class="btn btn-primary">Submit</button>
							</div>
						</div>
					</div>
					</form>
				</div>
			</div>
</body>
</html>
