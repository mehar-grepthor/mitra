<?php
require APPPATH . '/libraries/MY_REST_Controller.php';
require APPPATH . '/vendor/autoload.php';

use Firebase\JWT\JWT;
require_once(APPPATH."libraries/paytm/lib/config_paytm.php");
require_once(APPPATH."libraries/paytm/lib/encdec_paytm.php");
class Payment extends MY_REST_Controller
{
    public $checkSum;
    public function __construct()
    {
        parent::__construct();
        header("Pragma: no-cache");
        header("Cache-Control: no-cache");
        header("Expires: 0");
        $this->load->model('user_model');
        $this->load->model('wallet_transaction_model');
        $this->load->model('bank_details_model');
    }
    
    /**
     * @author Mehar
     * @desc To manage wallet amount
     * 
     */
    public function wallet_post($type = 'withdrawal'){
        $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        if($type == 'withdrawal'){
            $_POST = json_decode(file_get_contents('php://input'), TRUE);
            $source = NULL;
            if(! empty($_POST['AC'])){
                $source = $this->bank_details_model->insert([
                    'user_id' => $token_data->id,
                    'name' => $this->input->post('NAME'),
                    'ac' => $this->input->post('AC'),
                    'ifsc' => $this->input->post('IFSC'),
                    'bank_name' => $this->input->post('BANK_NAME'),
                ]);
            }
            $amount = floatval($this->input->post('TXNAMOUNT'));
            $id = $this->wallet_transaction_model->insert([
                'user_id' => $token_data->id,
                'type' => 'DEBIT',
                'cash' => $amount,
                'paytm' => (isset($_POST['PAYTM']))? $this->input->post('PAYTM') : NULL ,
                'upi' => (isset($_POST['UPI']))? $this->input->post('UPI') : NULL ,
                'bank_id' => $source,
                'order_id' => $this->input->post('ORDERID'),
                'description' => $this->input->post('DESC'),
            ]);
            if($id){
                $wallet = $this->user_model->where('id', $token_data->id)->fields('wallet')->as_array()->get();
                $this->user_model->update([
                    'id' => $token_data->id,
                    'wallet' =>  floatval($wallet['wallet']) - $amount
                ], 'id');
                $this->set_response(floatval($wallet['wallet'])- $amount, 'Wallet Updated', REST_Controller::HTTP_OK, TRUE);
            }else{
                $this->set_response(NULL, 'Internal Error Occured', REST_Controller::HTTP_OK, FALSE);
            }
        }elseif($type == 'history'){
            $data = $this->wallet_transaction_model->order_by('id', 'DESC')->where('user_id', $token_data->id)->get_all();
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }elseif ($type == 'plan_pay'){
            
        }
        
    }
    public function payment_success_post($gateway = 'paytm'){
        if($gateway == 'paytm'){
            $_POST = json_decode(file_get_contents('php://input'), TRUE);
            $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
            $amount = floatval($this->input->post('TXNAMOUNT'));
            $id = $this->wallet_transaction_model->insert([
                'user_id' => $token_data->id,
                'cash' => $amount,
                'type' => 'CREDIT',
                'txn_id' => $this->input->post('TXNID'),
                'bank_txn_id' => $this->input->post('BANKTXNID'),
                'order_id' => $this->input->post('ORDERID'),
                'resp_msg' => $this->input->post('RESPMSG'),
                'bank_name' => $this->input->post('BANKNAME'),
                'description' => $this->input->post('DESC'),
                'status' => 1
            ]);
            $wallet = $this->user_model->where('id', $token_data->id)->fields('wallet')->as_array()->get();
            
            if($id){
                $updated_wallet = $amount + $wallet['wallet'];
                $this->user_model->update([
                    'id' => $token_data->id,
                    'wallet' => $updated_wallet
                ], 'id');
                $this->set_response($updated_wallet, 'Wallet Updated', REST_Controller::HTTP_OK, TRUE);
            }
        }
        
    }
    public function payment_failure_post($gateway = 'paytm'){
        if($gateway == 'paytm'){
            $_POST = json_decode(file_get_contents('php://input'), TRUE);
            $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
            $amount = floatval($this->input->post('TXNAMOUNT'));
            $id = $this->wallet_transaction_model->insert([
                'user_id' => $token_data->id,
                'cash' => $amount,
                'type' => 'CREDIT',
                'txn_id' => $this->input->post('TXNID'),
                'bank_txn_id' => $this->input->post('BANKTXNID'),
                'order_id' => $this->input->post('ORDERID'),
                'resp_msg' => $this->input->post('RESPMSG'),
                'bank_name' => $this->input->post('BANKNAME'),
                'description' => $this->input->post('DESC'),
                'status' => 2
            ]);
            $wallet = $this->user_model->where('id', $token_data->id)->fields('wallet')->as_array()->get();
            
            if($id){
                /* $this->user_model->update([
                 'id' => $token_data->id,
                 'wallet' => $wallet['wallet'] - $amount
                 ], 'id'); */
                $this->set_response(NULL, 'Wallet Updated', REST_Controller::HTTP_OK, TRUE);
            }
        }
        
    }
    
    public function gen_checksum_post(){
        $_POST = json_decode(file_get_contents('php://input'), TRUE);
        $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        $findme   = 'REFUND';
        $findmepipe = '|';
        
        $paramList = array();
        
        $paramList["MID"] = $this->input->post('MID');
        $paramList["ORDER_ID"] = $this->input->post('ORDER_ID');
        $paramList["CUST_ID"] = $this->input->post('CUST_ID');
        $paramList["INDUSTRY_TYPE_ID"] = $this->input->post('INDUSTRY_TYPE_ID');
        $paramList["CHANNEL_ID"] = $this->input->post('CHANNEL_ID');
        $paramList["TXN_AMOUNT"] = $this->input->post('TXN_AMOUNT');
        $paramList["WEBSITE"] = $this->input->post('WEBSITE');
        $paramList["EMAIL"] = $this->input->post('EMAIL');
        $paramList["MOBILE_NO"] = $this->input->post('MOBILE_NO');
        $paramList["CALLBACK_URL"] = $this->input->post('CALLBACK_URL');
        
        foreach($_POST as $key=>$value)
        {
            $pos = strpos($value, $findme);
            $pospipe = strpos($value, $findmepipe);
            if ($pos === false || $pospipe === false)
            {
                $paramList[$key] = $value;
            }
        }
        
        $checkSum = getChecksumFromArray($paramList,PAYTM_MERCHANT_KEY);
        $this->response(array("CHECKSUMHASH" => $checkSum,"ORDER_ID" => $_POST["ORDER_ID"], "payt_STATUS" => "1"));
        //$this->set_response(array("CHECKSUMHASH" => $checkSum,"ORDER_ID" => $_POST["ORDER_ID"], "payt_STATUS" => "1"), 'Checksum Generated', REST_Controller::HTTP_OK, TRUE);
    }
    
    public function verify_checksum_post(){
        // $_POST = json_decode(file_get_contents('php://input'), TRUE);
        $paytmChecksum = "";
        $paramList = array();
        $isValidChecksum = FALSE;
        
        $paramList = $_POST;
        $return_array = $_POST;
        $paytmChecksum = isset($_POST["CHECKSUMHASH"]) ? $_POST["CHECKSUMHASH"] : ""; //Sent by Paytm pg
        
        //Verify all parameters received from Paytm pg to your application. Like MID received from paytm pg is same as your application’s MID, TXN_AMOUNT and ORDER_ID are same as what was sent by you to Paytm PG for initiating transaction etc.
        $isValidChecksum = verifychecksum_e($paramList, PAYTM_MERCHANT_KEY, $paytmChecksum); //will return TRUE or FALSE string.
        
        // if ($isValidChecksum===TRUE)
        // 	$return_array["IS_CHECKSUM_VALID"] = "Y";
        // else
        // 	$return_array["IS_CHECKSUM_VALID"] = "N";
        
        $return_array["IS_CHECKSUM_VALID"] = $isValidChecksum ? "Y" : "N";
        //$return_array["TXNTYPE"] = "";
        //$return_array["REFUNDAMT"] = "";
        unset($return_array["CHECKSUMHASH"]);
        
        $encoded_json = htmlentities(json_encode($return_array));
        $this->set_response_simple($return_array, 'Checksum Generated', REST_Controller::HTTP_OK, TRUE);
    }
    
    
}
