<?php
require APPPATH . '/libraries/MY_REST_Controller.php';
require APPPATH . '/vendor/autoload.php';

use Firebase\JWT\JWT;
class User extends MY_REST_Controller
{
    public $parent_id;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('member_model');
        $this->load->model('group_model');
        $this->load->model('user_model');
        $this->load->model('district_model');
        $this->load->model('scheme_model');
        $this->load->model('wallet_transaction_model');
        $this->load->model('user_scheme_subscription_model');
        $this->load->model('user_plan_subscriptions');
        $this->load->model('user_transaction_model');
        $this->load->model('payment_reminder_model');
        $this->load->model('gift_model');
        $this->parent_id = 1;
    }
    
    /**
     *@desc member crud
     *@author Trupti
     */
    public function member_post($method = 'c'){
        if($method == 'c'){
            $_POST = json_decode(file_get_contents("php://input"), TRUE);
            $this->form_validation->set_rules($this->member_model->rules);
            if ($this->form_validation->run() == FALSE) {
                $this->set_response_simple(validation_errors(), 'Internal Error Occured..!', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
            } else {
                $group = $this->group_model->where('name', 'user')->get();
                if(! empty($group)){
                    $member_id = $this->config->item('member_id', 'ion_auth');
                    $unique_id = generate_serial_no('MIT', 4, $member_id);
                    $this->db->where('id', 1);
                    $this->db->update('member_id',['member_id' => $member_id + 1]);
                    $email = strtolower($this->input->post('email'));
                    $identity = ($this->config->item('identity', 'ion_auth') === 'email') ? $email : $unique_id;
                    $additional_data = array(
                        'first_name' => $this->input->post('name'),
                        'unique_id' => $unique_id,
                        'active' => 1
                    );
                    $group_id[] = $group['id'];
                    $user_id = $this->ion_auth->register($identity, (empty($this->input->post('password')))? '123456': $this->input->post('password'),$this->input->post('email'), $additional_data, $group_id);
                    
                    $id = $this->member_model->insert([
                        'unique_id' => $unique_id,
                        'user_id' => $user_id,
                        'parent_user_id' => $this->parent_id,
                        'name' => $this->input->post('name'),
                        'mobile' => $this->input->post('mobile'),
                        'email' => $this->input->post('email'),
                        'state_id' => $this->input->post('state_id'),
                        'district_id' => $this->input->post('district_id'),
                        'scheme_id' => $this->input->post('scheme_id'),
                        'aadhar_no' => $this->input->post('aadhar_no'),
                        'pan' => $this->input->post('pan'),
                        'gender' => $this->input->post('gender'),
                        'member_dob' => $this->input->post('member_dob'),
                        'nominee_name' => $this->input->post('nominee_name'),
                        'nominee_aadhar' => $this->input->post('nominee_aadhar'),
                        'nominee_dob' => $this->input->post('nominee_dob'),
                        'relation' => $this->input->post('relation'),
                        'marital_status' => $this->input->post('marital_status'),
                        'status' => 1,
                    ]);
                   
                    $this->db->insert('user_schemes', ['user_id' => $id,'scheme_id' => $this->input->post('scheme_id')]);
                   
                    
                    $this->user_model->update([
                        'id' => $user_id,
                        'active' => 1
                    ], 'id');
                        if (!file_exists('uploads/person_image/')) {
                            mkdir('uploads/person_image/', 0777, true);
                        }
                        file_put_contents("./uploads/person_image/person_$id.jpg", base64_decode($this->input->post('person_img')));
                        $this->set_response_simple(NULL, 'Success..!', REST_Controller::HTTP_CREATED, TRUE);
                }else{
                    $this->set_response_simple(NULL, 'Member Group Not Found..!', REST_Controller::HTTP_INTERNAL_SERVER_ERROR, FALSE);
                }
            }
        }
    }
   
    /**
     * existing referal id validation
     * @param string $ref_id
     * @return boolean
     */
    public function check_referance($ref_id){
        if(! empty($ref_id)){
            $parent =  $this->user_model->fields('id, unique_id')->where('unique_id', $ref_id)->as_object()->get();
            if(! empty($parent)){
                $this->parent_id = $parent->id;
                return TRUE;
            }else{
                return FALSE;
            }
        }else{
            return TRUE;
        }
    }
    /**
     * @desc To subscribe a Scheme and transactions
     * @author Trupti
     */
    public function plan_subscriptions_post(){
        $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        $_POST = json_decode(file_get_contents('php://input'), TRUE);
        $this->form_validation->set_rules($this->user_plan_subscriptions->rules);
        if($this->form_validation->run() == FALSE){
            $this->set_response_simple(validation_errors(), 'Internal Error Occured..!', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
        }else{
            $is_plan_exist = $this->user_plan_subscriptions->where(['scheme_id' => $this->input->post('scheme_id'), 'user_id' => $token_data->id])->get();
            $user = $this->user_model->where('id', $token_data->id)->get();
            $admin = $this->user_model->where('id', 1)->get();
            $amount = floatval($this->input->post('amount'));
            if(empty($is_plan_exist)){
                $this->user_plan_subscriptions->insert([
                    'user_id' => $token_data->id,
                    'duration' => $this->input->post('duration'),
                    'scheme_id' => $this->input->post('scheme_id'),
                    'no_of_deposits' => 1,
                    'total_savings' => $amount
                ]);
              
            }else{
                $this->user_plan_subscriptions->update([
                    'no_of_deposits' => intval($is_plan_exist['no_of_deposits']) + 1,
                    'total_savings' => floatval($is_plan_exist['total_savings']) + $amount
                ], ['scheme_id' => $this->input->post('scheme_id'), 'user_id' => $token_data->id]);
            }
            
            $is_updated = $this->user_model->update([
                'id' => $token_data->id,
                'wallet' => $user['wallet'] - $amount
            ], 'id');
            
            $this->wallet_transaction_model->insert([
                'user_id' => $token_data->id,
                'type' => 'DEBIT',
                'cash' => $amount,
                'txn_id' => $this->input->post('txn_id'),
                'status' => 1
            ]);
            if(! empty($is_updated)){
                $is_updated = $this->user_model->update([
                    'id' => 1,
                    'wallet' => $admin['wallet'] + $amount
                ], 'id');
                
                $this->wallet_transaction_model->insert([
                    'user_id' => 1,
                    'type' => 'CREDIT',
                    'cash' => $amount,
                    'txn_id' => $this->input->post('txn_id'),
                    'status' => 1
                ]);
                
                $data = $this->user_transaction_model->insert([
                    'user_id' => $token_data->id,
                    'scheme_id' => $this->input->post('scheme_id'),
                    'amount' => $amount,
                    'txn_id' => $this->input->post('txn_id'),
                    'source' => 'Wallet'
                ]);
                $is_reminder_exist = $this->payment_reminder_model->where(['user_id' => $token_data->id, 'scheme_id' => $this->input->post('scheme_id'),])->get_all();
                if(empty($is_reminder_exist)){
                    $data = $this->payment_reminder_model->insert([
                        'user_id' => $token_data->id,
                        'scheme_id' => $this->input->post('scheme_id'),
                        'due_amount' => $amount,
                        'status' => 2
                    ]);
                }else{
                    $data = $this->payment_reminder_model->update([
                        'user_id' => $token_data->id,
                        'scheme_id' => $this->input->post('scheme_id'),
                        'due_amount' => $amount,
                        'status' => 2
                    ], ['scheme_id' => $this->input->post('scheme_id'), 'user_id' => $token_data->id, 'due_date' => $this->input->post('due_date')]);
                }
                $this->set_response_simple($data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
            }else{
                $this->set_response_simple(NULL, 'Internal Error Occured..!', REST_Controller::HTTP_CONFLICT, FALSE);
            }
        }
    }
    /**
     *@desc Notification
     *@author Trupti
     */
    public function notify_get(){
        //SELECT * FROM payment_reminders WHERE id IN ( SELECT MAX(id) FROM payment_reminders GROUP BY plan_details_id )
        $reminder_data = $this->db->query('SELECT payment_reminders.id, payment_reminders.user_id, payment_reminders.scheme_id, payment_reminders.due_amount, payment_reminders.due_date, payment_reminders.status, users.unique_id, schemes.name as plan_name FROM payment_reminders LEFT JOIN users ON users.id = payment_reminders.user_id LEFT JOIN schemes ON schemes.id = payment_reminders.scheme_id WHERE payment_reminders.id IN ( SELECT MAX(id) FROM payment_reminders GROUP BY  payment_reminders.user_id,payment_reminders.scheme_id )')->result_array();
        $current_date = date_create(date('Y-m-d'));
        foreach ($reminder_data as $data){
            $notify_date = date_create(date('Y-m-d', strtotime($data['due_date']. ' + 30 days')));
            if(date_diff($current_date,$notify_date)->days == 0){
                $this->payment_reminder_model->insert([
                    'user_id' => $data['user_id'],
                    'scheme_id' => $data['scheme_id'],
                    'due_amount' => $data['due_amount'],
                    'due_date' => date('Y-m-d'),
                    'status' => 1
                ]);
            }
        }
        $this->set_response_simple($reminder_data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    }
    /**
     *@desc Payment reminders
     *@author Trupti 
     */
    public function reminders_get(){
        $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        $data = $this->payment_reminder_model->where(['user_id' => $token_data->id, 'status' => 1])->get_all();
        $this->set_response_simple($data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    }
    
    /**
     *@desc Check whether scheme exists or not
     *@author Trupti
     */
    public function is_plan_exist_get($scheme_id = NULL){
        $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        if(empty($scheme_id)){
            $is_plan_exist = $this->user_plan_subscriptions->where(['user_id' => $token_data->id])->get_all();
            if($is_plan_exist){
                $this->set_response_simple($is_plan_exist, 'Success..!', REST_Controller::HTTP_OK, TRUE);
            }else{
                $this->set_response_simple(FALSE, 'Success..!', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, TRUE);
            }
        }else{
            $is_plan_exist = $this->user_plan_subscriptions->where(['scheme_id' => $scheme_id, 'user_id' => $token_data->id])->get();
            if(empty($is_plan_exist)){
                $this->set_response_simple(FALSE, 'Success..!', REST_Controller::HTTP_OK, TRUE);
            }else {
                $is_plan_exist['transactions'] = $this->user_transaction_model->where(['scheme_id' => $scheme_id, 'user_id' => $token_data->id])->get_all();
                $this->set_response_simple($is_plan_exist, 'Success..!', REST_Controller::HTTP_OK, TRUE);
            }
        }
    }
    
    /**
     *@desc Gift
     *@author Trupti
     */
    public function gift_get(){
        $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        $data = $this->gift_model->where('user_id', $token_data->id)->get_all();
        if(! empty($data)){
            for ($i = 0; $i < count($data) ; $i++){
                $data[$i]['image'] = base_url().'uploads/gift_image/gift_'.$data[$i]['id'].'.jpg';
            }
        }
        $this->set_response_simple($data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    }
    
    /**
     *@desc Tree
     *@author Trupti
     */
    public function tree_mitra_get(){
        $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        $data = $this->member_model->fields('id', 'user_id')->where('user_id', $token_data->id)->with_user('fields:id, unique_id, email, first_name')->with_parent('fields: unique_id, email, first_name')->get_all();
        $data['childs'] = $this->member_model->where('parent_user_id', $token_data->id)->with_user('fields:first_name')->get_all();
        $this->set_response_simple($data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        
    }
    /**
     *@desc get Gift list and update gift status
     *@author Trupti
     */
    public function gift_crud_get($method = 'r',$target = NULL){
        $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        if($method == 'r'){
            $data = $this->gift_model->where('user_id', $token_data->id)->get_all();
            if(! empty($data)){
                for ($i = 0; $i < count($data) ; $i++){
                    $data[$i]['image'] = base_url().'uploads/gift_image/gift_'.$data[$i]['id'].'.jpg';
                }
            }
            $this->set_response_simple($data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }elseif($method == 'u'){
            $data = $this->gift_model->update([
                'id'=>$target,
                'user_id' => $token_data->id,
                'status' =>2
            ], 'id');
            $this->set_response_simple($data, 'Updated..!', REST_Controller::HTTP_OK, TRUE);
        }
       
    }
}

