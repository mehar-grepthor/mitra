<?php
require APPPATH . '/libraries/MY_REST_Controller.php';
require APPPATH . '/vendor/autoload.php';

use Firebase\JWT\JWT;
class Mlm extends MY_REST_Controller
{
    public $parent_id;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('member_model');
        $this->load->model('group_model');
        $this->load->model('user_model');
        $this->load->model('district_model');
        $this->load->model('plan_model');
        $this->load->model('plan_details_model');
        $this->load->model('user_plan_subscriptions');
        $this->load->model('user_transaction_model');
        $this->load->model('wallet_transaction_model');
        $this->load->model('payment_reminder_model');
        $this->parent_id = 1;
    }
    
    public function join_mlm_post(){
        $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        $_POST = json_decode(file_get_contents("php://input"), TRUE);
        $this->form_validation->set_rules($this->member_model->rules['mlm_member']);
        if ($this->form_validation->run() == FALSE) {
            $this->set_response_simple(validation_errors(), 'Internal Error Occured..!', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
        } else {
            $user = $this->user_model->where('id', $token_data->id)->as_array()->get();
            $admin = $this->user_model->where('id', 1)->as_array()->get();
            $amount = floatval($this->input->post('amount'));
            $is_updated = $this->user_model->update([
                'id' => $token_data->id,
                'wallet' => $user['wallet'] - $amount
            ], 'id');
            
            $this->wallet_transaction_model->insert([
                'user_id' => $token_data->id,
                'type' => 'DEBIT',
                'cash' => $amount,
                'txn_id' => $this->input->post('txn_id'),
                'status' => 1
            ]);
            if(! empty($is_updated)){
                $is_updated = $this->user_model->update([
                    'id' => 1,
                    'wallet' => $admin['wallet'] + $amount
                ], 'id');
                
                $this->wallet_transaction_model->insert([
                    'user_id' => 1,
                    'type' => 'CREDIT',
                    'cash' => $amount,
                    'txn_id' => $this->input->post('txn_id'),
                    'status' => 1
                ]);
                $data = $this->member_model->update([
                    'user_id' => $token_data->id,
                    'is_mlm_user' => 1,
                    'mlm_parent_id' => $this->parent_id
                ], 'user_id');
            }
            $this->set_response_simple($data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }
    }
    
    public function is_mlm_user_get(){
        $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        $data = $this->member_model->fields('id, user_id, is_mlm_user')->where('user_id', $token_data->id)->get();
        if($data['is_mlm_user']){
           $status = TRUE; 
        }else{
            $status = FALSE; 
        }
        $this->set_response_simple($status, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    }
    
    /**
     * @desc To get tree details
     * @author Mehar
     */
    public function mlm_tree_get($target = NULL){
        $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        if(empty($target)){
            $tree = $this->member_model->fields('id, user_id')->where('user_id', $token_data->id)->with_user('fields: unique_id, first_name, last_name, email, phone, wallet')->with_groups('fields: id, name, priority')->with_parent('fields: unique_id, first_name, last_name, email, phone, wallet')->get_all();
            $savings = $this->db->query("SELECT SUM(`total_savings`) as savings FROM `user_plan_subscriptions` WHERE user_id IN (SELECT `user_id` FROM `members` WHERE `parent_id` = $token_data->id OR user_id = $token_data->id) AND plan_id IN(4)")->row()->savings;
            $tree['total_savings'] = (empty($savings))? 0: $savings;
            $tree['childs'] = $this->member_model->fields('id, user_id, mlm_parent_id')->where('mlm_parent_id', $token_data->id)->with_user('fields: unique_id, first_name, last_name, email, phone, wallet')->get_all();
            $tree['no_of_childs'] = count($tree['childs']);
            if(! empty($tree['childs'])){
                for($i = 0; $i < count($tree['childs']); $i++){
                    $tree['childs'][$i]['total_savings'] = $this->db->query("SELECT SUM(`total_savings`) as savings FROM `user_plan_subscriptions` WHERE user_id IN (SELECT `user_id` FROM `members` WHERE `parent_id` = ".$tree['childs'][$i]['user_id']." OR user_id = ".$tree['childs'][$i]['user_id'].") ")->row()->savings;
                    $tree['childs'][$i]['no_of_childs'] = $this->db->query("SELECT count(*) from members where mlm_parent_id = "+ $tree['childs'][$i]['user_id']);
                    $tree['no_of_childs'] + $tree['childs'][$i]['no_of_childs'];
                }
            }
        }else{
            $tree = $this->member_model->fields('id, user_id')->where('user_id', $target)->with_user('fields: unique_id, first_name, last_name, email, phone, wallet')->with_groups('fields: id, name, priority')->with_parent('fields: unique_id, first_name, last_name, email, phone, wallet')->get_all();
            $savings = $this->db->query("SELECT SUM(`total_savings`) as savings FROM `user_plan_subscriptions` WHERE user_id IN (SELECT `user_id` FROM `members` WHERE `parent_id` = $target OR user_id = $target)  AND plan_id IN(4)")->row()->savings;
            $tree['total_savings'] = (empty($savings))? 0: $savings;
            $tree['childs'] = $this->member_model->fields('id, user_id, mlm_parent_id')->where('mlm_parent_id', $target)->with_user('fields: unique_id, first_name, last_name, email, phone, wallet')->get_all();
            $tree['no_of_childs'] = count($tree['childs']);
            if(! empty($tree['childs'])){
                for($i = 0; $i < count($tree['childs']); $i++){
                    $tree['childs'][$i]['total_savings'] = $this->db->query("SELECT SUM(`total_savings`) as savings FROM `user_plan_subscriptions` WHERE user_id IN (SELECT `user_id` FROM `members` WHERE `parent_id` = ".$tree['childs'][$i]['user_id']." OR user_id = ".$tree['childs'][$i]['user_id'].") ")->row()->savings;
                    $tree['childs'][$i]['no_of_childs'] = $this->db->query("SELECT count(*) from members where mlm_parent_id = "+ $tree['childs'][$i]['user_id']);
                    $tree['no_of_childs'] + $tree['childs'][$i]['no_of_childs'];
                }
            }
        }
        $this->set_response_simple($tree, 'Success..!', REST_Controller::HTTP_OK, TRUE);
    }
    
    
    /**
     * existing referal id validation
     * @param string $ref_id
     * @return boolean
     */
    public function check_mlm_referance($ref_id){
        if(! empty($ref_id)){
            $parent =  $this->user_model->fields('id, unique_id')->where('unique_id', $ref_id)->as_object()->get();
            if(! empty($parent)){
                $status = $this->db->query("SELECT * FROM `members` WHERE is_mlm_user = 1 AND (SELECT COUNT(mlm_parent_id) FROM members WHERE mlm_parent_id = ".$parent->id.") < 5")->result_array();
                if(! empty($status)){
                    $this->parent_id = $parent->id;
                    return TRUE;
                }else{
                    return FALSE;
                }
            }else{
                return FALSE;
            }
        }else{
            return TRUE;
        }
    }
    
}

